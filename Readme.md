# Date Picker Project

**Important:** Please see our base [developer guidelines](https://gitlab.com/shaneneal_/developer-guidelines).

## Details

Example/Sample Links

https://uxdesign.cc/rethinking-the-date-picker-ui-99b9dcb303ad

https://rajasegar.github.io/react-awesome-datepicker-demo/


## Design

![](./date-picker.png)

## Functionality

* Replicate the core functionality of the Example React date picker for Vue2
  * Consistent Date, Month, Year selection
  * Select Dates will restrict the Months and Years available eg. 31/02 or 29/02/2021 (leap year) will not be possible
* Ability to type input the date
  * 01031997 will output the date as 01/03/1997
* Option to select the input date, month or year to allow changing of that value (optional)
* Ability to set defaults for the date field eg.
  * Year range - +-40 for year ranges 40 years into the future and past from current year
  * Year range +20 for year ranges 20 years into the future from current year
  * Year range =-18 for year ranges 18 years or older from current year (Date of Birth default for Wineries so you can only select dates that are 18 years or older)
  * Default date - today which presets the date on the date field to the current date
* Ability to toggle the use of the single date picker mode
  *  Full - Type and touch options
  *  Simple - Touch options only
  *  Type - Type options only
* Toggle off Date, Month or Year to allow the same component to be reused for the following configurations
  *  Month, Year
  *  Date, Month
  *  Month
  *  Year


## Developer Notes

* Should be able to be compiled as an npm package. (We will have this in our private npm registry)
* Config options listed above sould be able to be passed into the component
* Should be tested on both desktop and mobile browsers for functionality.
